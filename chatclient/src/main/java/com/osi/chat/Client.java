package com.osi.chat;

public class Client
{
	static DataHandler DATA_HANDLER; 


	public static void main(String args[]) {
		
		// Creates the data handler
		DATA_HANDLER = new DataHandler(); 
		
		// Creates the main window
		(new MainWindow()).setVisible(true);
	}
}
