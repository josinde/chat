package com.osi.chat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



public class DataHandler
{
    /** Active windows */
    private HashMap<String, Window> _windowList;

    /** Pending messages */
    private ArrayList<Message> _outputMessageList;

    /** Just the users table */
    private DataHandlerListener _listener;



    /** Constructor */
    DataHandler() {
        _windowList = new HashMap<String, Window>();
        _outputMessageList = new ArrayList<Message>();
    }

    public void addListener(DataHandlerListener listener) {
        _listener = listener;
    }

    /** 
     * The main app request a window to the DataHAndler
     * Create a new window if one doesn't exist before
     */
    public Window getUserWindow(String user) {
        // Check that it is not already in the system
        Window window = _windowList.get(user);
        if (window == null) {
            // Creates a new one and register it into the register
            window = new Window(user, this);
            _windowList.put(user, window);
        }
        // Ensure it is visible:
        // This can be the first instance or it could even
        // be closed by the remote user
        if (!window.isVisible()) {
            window.setVisible(true);
        }

        return window;
    }

    /** Task -> DATA HANDLER */
    public void updateUserList(String[] users) {
        // Notifies its only listener (the users table in the main window)
        _listener.updateUserList(users);

        // Also check al the active windows to mark those active/inactives
        for (Window window : _windowList.values()) {
            boolean active = isIncluded(window.getUser(), users);
            window.setActive(active);
        }
    }

    private boolean isIncluded(String user, String[] userList) {
        for (String u : userList) {
            if (user.equals(u)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Print a message into a window
     * Task -> DATA HANDLER -> user window
     */
    public void printMessage(String user, String msg) {
        //System.out.println(user + ": " + msg);

        // Find the proper window
        Window window = getUserWindow(user);
        // Send the message to the window
        window.addRemoteMessage(msg);
    }

    /** 
     * Store messages to be sent 
     * Uuser window -> DATA HANDLER
     */
    public void queueMessage(String user, String msg) {
        _outputMessageList.add(new Message(user, msg));
    }

    /** 
     * Deliver pending messages 
     * Task <- DATA HANDLER
     */
    @SuppressWarnings("unchecked")
    public List<Message> getMessages() {
        List<Message> res = (List<Message>) _outputMessageList.clone();
        _outputMessageList.clear();
        return res;
    }

    /** Log any error to the main window */
    public void log(String errorMsg) {
        _listener.log(errorMsg);
    }
}
