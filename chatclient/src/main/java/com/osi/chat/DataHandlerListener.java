package com.osi.chat;


interface DataHandlerListener
{
	public void updateUserList(String[] users);
    
    public void log(String msg);
}
