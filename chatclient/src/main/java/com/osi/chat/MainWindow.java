package com.osi.chat;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;



class MainWindow extends JFrame implements DataHandlerListener
{
	private Task _connectionTask;

	private JTextField _userField;
	private JTextField _serverField;
	private UsersTable _usersTable;
	private JTextArea _logsTextArea;
	
	
	MainWindow() {
		
		try {
			
            // Register in the DataHandler as client
            Client.DATA_HANDLER.addListener(this);

			// Layout
			initLayout();
			
			// Centers it
			setLocationRelativeTo(null);
			
			// Close action
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private void initLayout() throws Exception {
		this.setTitle("HTTP Chat");
		this.setSize(new Dimension (350, 200));

		// Tabbed pane
		JTabbedPane tabbedPane = new JTabbedPane();
		this.getContentPane().add(tabbedPane);
		
		// ConnectionPane
		tabbedPane.add("server", getConnectionPane());
		tabbedPane.add("users", getUsersPane());
        tabbedPane.add("logs", getLogsPane());
	}
	
	private JComponent _connectionPane;
	
	private JComponent getConnectionPane() {
		if (_connectionPane == null) {
			_connectionPane = new JPanel();
			_connectionPane.setLayout(new GridBagLayout());
			
			JLabel userLabel = new JLabel("User:");
			_connectionPane.add(userLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, 
	        GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(20, 20, 4, 2), 0, 0));

			_userField = new JTextField();
			_connectionPane.add(_userField, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, 
	        GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(20, 2, 4, 20), 0, 0));

			JLabel serverLabel = new JLabel("Server:");
			_connectionPane.add(serverLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, 
	        GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(4, 20, 4, 2), 0, 0));

			_serverField = new JTextField("http://localhost:8080/chatserver/ChatServer");
			_connectionPane.add(_serverField, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, 
	        GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(4, 2, 4, 20), 0, 0));

			JButton connectButton = new JButton("CONNECT");
			connectButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ev) {
					// Close any previous task
					if (_connectionTask != null) {
						_connectionTask.close();
					}
					
					// New parameters
					String user = _userField.getText().trim();
					String server = _serverField.getText().trim();
					
					// Creates the connection thread
					_connectionTask = new Task(user, server);
					_connectionTask.start();
				}
			});
			_connectionPane.add(connectButton, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, 
	        GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(4, 2, 20, 20), 0, 0));
			
		}
		return _connectionPane;
	}
	
	private JComponent _usersPane;
	
	private JComponent getUsersPane() {
		if (_usersPane == null) {
			_usersPane = new JPanel();
			_usersPane.setLayout(new GridBagLayout());
			_usersTable = new UsersTable();
			JScrollPane scroll = new JScrollPane(_usersTable);
			_usersPane.add(scroll, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, 
	        GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
		}
		return _usersPane;
	}

    private JComponent _logsPane;

    private JComponent getLogsPane() {
        if (_logsPane == null) {
            _logsPane = new JPanel();
            _logsPane.setLayout(new GridBagLayout());
            _logsTextArea = new JTextArea();
            JScrollPane scroll = new JScrollPane(_logsTextArea);
            _logsPane.add(scroll, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, 
            GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 2, 10), 0, 0));
            JButton cleanButton = new JButton("CLEAN");
            cleanButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    _logsTextArea.setText("");
                }
            });
            _logsPane.add(cleanButton, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, 
                    GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 10, 2, 10), 0, 0));
        }
        return _logsPane;
    }

    // DATA HANDLER LISTENER IFCE ----------------------------------------------
    
    public void updateUserList(String[] users) {
        // Forwards the call to the users table
        _usersTable.updateUserList(users);
    }
    
    // Timestamp formatter
    private DateFormat _formatter = DateFormat.getTimeInstance(DateFormat.MEDIUM);
    
    public void log(String msg) {
        // Timestamp
        String ts = _formatter.format(new Date());
        _logsTextArea.append(ts + ":\n");
        // Register the message into the logs window
        _logsTextArea.append(msg);
    }
}
