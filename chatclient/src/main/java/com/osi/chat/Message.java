package com.osi.chat;

class Message
{
	String user;
	String text;
	
	Message(String to, String text) {
		this.user = to;
		this.text = text;
	}
}
