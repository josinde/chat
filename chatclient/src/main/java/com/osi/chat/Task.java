package com.osi.chat;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;



public class Task extends Thread
{
    private static long TICKS = 1000L;

    private String _user;
    private String _server;

    private volatile boolean _done;

    public Task(String user, String url) {
        _user = user;
        _server = url;

        _done = false;
    }

    public void run() {

        while (!_done) try {

            // Wait for TICKS ms
            try {Thread.sleep(TICKS);} catch (Exception ex) { }

            // Create an instance of HttpClient.
            HttpClient client = HttpClientBuilder.create().build();

            // Create a method instance.
            HttpPost post = new HttpPost(_server);

            // Header
            post.setHeader("User-Agent", "custom-java-app");

            // Set the user
            List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
            urlParameters.add(new BasicNameValuePair("USER", _user));

            // Read pending messages
            List<Message> messages = Client.DATA_HANDLER.getMessages();

            // Store them as parameters into the request
            if (!messages.isEmpty()) {
                for (Message msg : messages) {
                    urlParameters.add(new BasicNameValuePair(msg.user, msg.text));
                }
            }

            post.setEntity(new UrlEncodedFormEntity(urlParameters));

            // Execute the method.
            HttpResponse response = client.execute(post);

            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                reportException("Method failed: " + response.getStatusLine(), null);
            }

            // Read the response body and parse it
            HttpEntity entity = response.getEntity();
            InputStream inputStream = entity.getContent();
            //Charset charset = Charset.forName("UTF-8");
            Charset charset = Charset.forName("ISO-8859-1");
            try (BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream, charset))) {

                StringBuffer result = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line).append('\n');
                }
                parseData(result.toString());
            }

        } catch (IOException e) {
            reportException("Fatal transport error: " + e.getMessage(), e);
        }
    }

    private void parseData(String is) {
        // Wraps the is
        StringReader sr = new StringReader(is);
        BufferedReader reader = new BufferedReader(sr);

        try
        {
            String line = null;
            while((line = reader.readLine()) != null) {
                //System.out.println("LINE: " + line);
                String[] element = line.split("\t");
                if (element[0].equals("USERS")) {
                    // Users list
                    //String[] users = Arrays.copyOfRange(element, 1, element.length); JDK1.6
                    String[] users = new String[element.length - 1];
                    for (int i = 0; i < users.length; ++i) {
                        users[i] = element[i + 1];
                    }
                    Client.DATA_HANDLER.updateUserList(users);
                } else if (element.length == 2) {
                    String user = element[0];
                    String text = element[1];
                    Client.DATA_HANDLER.printMessage(user, text);
                } else {
                    // Problems
                    continue;
                }
            }

        } catch (IOException ex)	{	
            reportException("IOException parsing the data: " + ex.getMessage(), ex);
        }
    }

    private void reportException(String msg, Exception ex) {
        Client.DATA_HANDLER.log(msg + "\n");
        if (ex != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(baos);
            ex.printStackTrace(ps);
            Client.DATA_HANDLER.log("\n\n" + baos.toString() + "\n\n");
        }
    }

    public void close() {
        _done = true;
    }
}
