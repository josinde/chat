package com.osi.chat;

import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;



class UsersTable extends JTable
{
	private MyTableModel _model;
	
	UsersTable() {
		
	    // Local model 
	    _model = new MyTableModel();
	    setModel(_model);

	    // Properties
	    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

	    // Set a cell editor that in fact open a wondow
	    this.setDefaultEditor(String.class, new DefaultCellEditor(new JTextField()) {

	        @Override
	        public int getClickCountToStart() {
	            return 2;
	        }

	        @Override
	        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
	            // Abre la ventana correspondiente (la crea si hace falta)
	            String user = (String) value;
	            Client.DATA_HANDLER.getUserWindow(user);

	            // Cancels the edition
	            //this.stopCellEditing();
	            return null;
	        }
	    });
	}

	public void updateUserList(String[] users) {
		_model.update(users);
	}
	
	
	class MyTableModel extends AbstractTableModel {
		
		private String[] columnNames = new String[] {"active users"};
		private Class<?>[] columnTypes = new Class<?>[] {String.class};
		
		private String[] data = new String[0];

		public int getColumnCount() {
			return columnNames.length;
		}

		public int getRowCount() {
			return data.length;
		}

		public String getColumnName(int col) {
			return columnNames[col];
		}

		public Object getValueAt(int row, int col) {
			return data[row];
		}

		public Class<?> getColumnClass(int c) {
			return columnTypes[c];
		}

		/*
		 * Don't need to implement this method unless your table's
		 * editable.
		 */
		public boolean isCellEditable(int row, int col) {
			return true;
		}

		private void update(String[] users) {
			data = users; 
			fireTableDataChanged();
		}
	}
}
