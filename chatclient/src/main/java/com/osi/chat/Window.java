package com.osi.chat;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;



public class Window extends JFrame
{

    private static Color ACTIVE_COLOR = new Color(255, 255, 240);

    private DateFormat _formatter;

    private DataHandler _handler;
    private String _user;

    // Panels
    private JTextPane _pane;
    private JTextArea _input;

    private boolean _active;


    Window(String user, DataHandler handler) {

        // Always on top
        this.setAlwaysOnTop(true);

        // Timestamp formatter
        _formatter = DateFormat.getTimeInstance(DateFormat.MEDIUM);

        _user = user;
        _handler = handler;

        _active = true;

        try {

            // Layout
            initLayout();

            // Close action
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void initLayout() throws Exception {
        this.setTitle(_user);
        this.setSize(new Dimension (300, 600));
        this.getContentPane().setLayout(new GridBagLayout());

        JSplitPane spane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        this.getContentPane().add(spane, new GridBagConstraints(0, 0, 2, 1, 1.0, 1.0, 
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(4, 4, 2, 4), 0, 0));
        spane.setDividerLocation(580);
        spane.setDividerSize(2);

        // UpperPanel
        _pane = new JTextPane();
        _pane.setBackground(ACTIVE_COLOR);
        _pane.setEditable(false);
        // Setup initial styles
        StyledDocument doc1 = _pane.getStyledDocument();
        addStylesToDocument(doc1);

        JScrollPane scroll1 = new JScrollPane(_pane);
        spane.setTopComponent(scroll1);

        // LowerPanel
        _input = new JTextArea();
        _input.setLineWrap(true);

        // Input listener
        // Takes care of the text entered into the input field
        _input.getDocument().addDocumentListener(new DocumentListener() {

            public void changedUpdate(DocumentEvent e) { }

            public void insertUpdate(DocumentEvent e) {
                //System.out.println(e);
                try {
                    int offset = e.getOffset();

                    Document doc = _input.getDocument();
                    String msg = doc.getText(offset, 1);
                    int docLength = doc.getLength();

                    // Press return at the end of the line
                    if (msg.equals("\n") && offset == (doc.getLength() - 1)) {
                        // Send the message remotely 
                        // and writes a local copy in the screen area
                        msg = doc.getText(0, docLength);
                        sendMessage(msg);						 

                        // Clear the input
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                Document doc = _input.getDocument();
                                try {
                                    doc.remove(0, doc.getLength());
                                } catch (BadLocationException e) {}
                            }
                        });
                    }

                } catch (BadLocationException e1) { }
            }

            public void	removeUpdate(DocumentEvent e) { }
        });

        JScrollPane scroll2 = new JScrollPane(_input);
        spane.setBottomComponent(scroll2);

        // BEEP  BUTTON
        JButton beepButton = new JButton("BELL");
        beepButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                // Send a WARNING message remotely
                sendWarning();
            }
        });
        this.getContentPane().add(beepButton, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, 
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 4, 4, 4), 0, 0));

        // CLEAN BUTTON
        JButton cleanButton = new JButton("BORRA PANEL");
        cleanButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                cleanScreen();
            }
        });
        this.getContentPane().add(cleanButton, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, 
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 4, 4, 4), 0, 0));
    }

    /** Send a warning sound to the remote user */	
    private void sendWarning() {
        // Print the "BEEP" message in the local window
        String msg = "(Sending a call to '" + _user + "')\n";
        addLocalMessage(msg);
        // Send the warning message to the remote client
        _handler.queueMessage(_user, "WARNING:");
    }

    /** Cleans the message screen */
    private void cleanScreen() {
        try {
            Document doc = _pane.getDocument();
            doc.remove(0, doc.getLength());
        } catch (BadLocationException e) { }
    }

    @Override
    public void setVisible(boolean visible) {
        // Warm the user
        if (visible && !isShowing()) {
            Toolkit.getDefaultToolkit().beep();
        }
        super.setVisible(visible);
    }

    public String getUser() {
        return _user;
    }

    public void addRemoteMessage(String msg) {
        StyledDocument doc = _pane.getStyledDocument();
        try {
            // Check that it is not a TS message
            String tsMsg = getTSMessage(msg);
            if (tsMsg != null) {
                // Timestamp
                doc.insertString(doc.getLength(), tsMsg, doc.getStyle("timestamp")); addLog(tsMsg);
                _pane.setCaretPosition(doc.getLength());
                return;
            }

            // Maybe a warning with a message
            String warningMsg = getWarningMessage(msg);
            if (warningMsg != null) {
                // Play a sound to warm the user
                playWarningSound();
                // Also show the warning on the screen
                doc.insertString(doc.getLength(), warningMsg, doc.getStyle("italic")); addLog(warningMsg);
                _pane.setCaretPosition(doc.getLength());
                return;
            } 

            // Regular message
            String m = "- " + msg + "\n";
            doc.insertString(doc.getLength(), m, doc.getStyle("remote")); addLog(m);
            _pane.setCaretPosition(doc.getLength());

        } catch (BadLocationException ble) {
            System.err.println("Couldn't insert initial text into text pane.");
        } catch (IOException e) {
            System.err.println("Couldn't play warning sound.");
        }
    }

    private void addLocalMessage(String msg) {
        StyledDocument doc = _pane.getStyledDocument();
        try {
            String m = "- " + msg; // + "\n"; My messages includes always the cr
            doc.insertString(doc.getLength(), m, doc.getStyle("local")); addLog(m);
            _pane.setCaretPosition(doc.getLength());

        } catch (BadLocationException ble) {
            System.err.println("Couldn't insert initial text into text pane.");
        }
    }



    private void addLog(String msg) {
        try {
            // Gets the file
            PrintStream ps = getLogStream();
            // Remove '\n's
            msg = msg.replaceAll("\\n", "");
            // Write into the log
            ps.println(msg);
            ps.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
            _logFile = null;
        }
    }

    private PrintStream _logFile;

    private PrintStream getLogStream() {

        if (_logFile == null) try {
            // Filename
            String logName = System.getProperty("user.home") + File.separator + _user + ".log";
            System.out.println("log: " + logName);
            // Open the stream
            boolean append = true;
            _logFile = new PrintStream(new BufferedOutputStream(new FileOutputStream(new File(logName), append)));
            // First log
            DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
            String msg = "Log session: " + formatter.format(new Date());

            _logFile.println();
            _logFile.println(msg);
            _logFile.println();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return _logFile;
    }

    private void sendMessage(String msg) {
        // Print the message
        addLocalMessage(msg);

        // Send it to the server (without the carriage return)
        String m = msg.substring(0, msg.length() - 1);
        _handler.queueMessage(_user, m);
    }

    /** 
     * Returns a timestamp pair if possible 
     * In any other case it returns null
     */
    private String getTSMessage(String msg) {
        if (msg.startsWith("TS:")) try {

            String[] element = msg.split(":");
            long ts1 = Long.parseLong(element[1]);
            String s1 = _formatter.format(new Date(ts1));
            long ts2 = Long.parseLong(element[2]);
            String s2 = _formatter.format(new Date(ts2));
            //String text = "  " + "(" + s1 + " -> " + s2 + ")" + "\n";
            String text = "  " + s1 + "\n\n" + "  " + s2 + "\n"; 
            return text;

        } catch (Exception ex) {
            // NumberFormatException, ArrayOutOfBoundsException, ...
        }

        // Any other case, including errors, return null
        return null;
    }

    private String getWarningMessage(String msg) {
        if (msg.startsWith("WARNING:")) {
            // Yes it is a warning message
            String[] element = msg.split(":");
            if (element.length > 1) {
                // It also includes a custom message on it
                return ("  " + element[1] + "\n");
            } else {
                // No, just a warning message
                return "  Beep!\n";
            }

        } else {
            // Not a warning message
            return null;
        }
    }

    protected void addStylesToDocument(StyledDocument doc) {
        //Initialize some styles.
        Style def = StyleContext.getDefaultStyleContext().
                getStyle(StyleContext.DEFAULT_STYLE);

        Style regular = doc.addStyle("regular", def);
        StyleConstants.setFontFamily(def, "SansSerif");

        Style s = doc.addStyle("italic", regular);
        StyleConstants.setItalic(s, true);

        s = doc.addStyle("bold", regular);
        StyleConstants.setBold(s, true);

        s = doc.addStyle("small", regular);
        StyleConstants.setFontSize(s, 10);

        s = doc.addStyle("large", regular);
        StyleConstants.setFontSize(s, 16);

        s = doc.addStyle("local", regular);
        StyleConstants.setForeground(s, new Color(70, 70, 70));

        s = doc.addStyle("remote", regular);
        StyleConstants.setForeground(s, new Color(50, 50, 200));

        s = doc.addStyle("timestamp", regular);
        StyleConstants.setForeground(s, new Color(30, 150, 30));
    }

    private boolean _blink = false;

    public void setActive(boolean active) {
        // Report a change in the state
        if (_active != active) {
            _active = active;
            // Update the title
            this.setTitle((_active) ? _user : "* " + _user + " *");
        }

        // Flip-flop (when inactive user)
        if (active) {
            _blink = false;
        } else {
            this.setTitle((_blink) ? _user : "* " + _user + " *");
            _blink = !(_blink);
        }
    }

    private void playWarningSound() throws IOException {
        // Create an AudioStream object from the input stream.
        //InputStream SOUND_STREAM = getClass().getResourceAsStream("BasketballBuzzer.mp3");
        InputStream SOUND_STREAM = getClass().getResourceAsStream("Ring.mid");
        AudioStream as = new AudioStream(SOUND_STREAM);
        // Use the static class member "player" from class AudioPlayer to play clip
        AudioPlayer.player.start(as);            
    }
}
