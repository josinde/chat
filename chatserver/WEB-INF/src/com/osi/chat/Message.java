package com.osi.chat;

class Message
{
	private String _user;
	private String _text;
	
	Message(String from, String text) {
		_user = from;
		_text = text;
	}
	
	public String getUser() {
		return _user;
	}
	
	public String getText() {
		return _text;
	}

	@Override
	public String toString() {
		return _user + "\t" + _text;
	}
}
