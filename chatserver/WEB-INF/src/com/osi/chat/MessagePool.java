package com.osi.chat;

import java.util.ArrayList;
import java.util.List;

class MessagePool
{
	/** MAX INTERVAL BETWEEN MESSAGES */
	private static long MAX_INTERVAL = 10 * 1000L;
	/** */
	private long _lastTimestamp;

	/** Pool os messages */
	private ArrayList<Message> _messages;
	
	
	/** Constructor */
	MessagePool() {
		_messages = new ArrayList<Message>();
	}
	
	public void addMessage(Message msg) {
		// Timestamp
		long ts = System.currentTimeMillis();
		
		// Already other messages?
		if (_lastTimestamp != 0) {
			// Compare timestamps
			// TODO: This should be done for every users
			if ((ts - _lastTimestamp) > MAX_INTERVAL) {
				// Add an additional message
				String text = "TS:" + Long.toString(_lastTimestamp) + ":" + Long.toString(ts);
				//System.out.println("ssss " + text);
				_messages.add(new Message(msg.getUser() ,text));
			}
		}
		// Updates last timestamp
		_lastTimestamp = ts;
		
		// Add the message to the pool
		_messages.add(msg);
	}
	
	public List<Message> getMessages() {
		//return (List<Message>) _messages.clone();
		return _messages; // Optimization
	}
	
	public void clear() {
		_messages.clear();
	}
}
