package com.osi.chat;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Server extends HttpServlet
{
    private static long TICKS = 1000L;


    /** Active user list */
    private Hashtable<String, User> _users;

    /** Channels */
    private Hashtable<String, MessagePool> _channels;

    /** Users update task */
    private UserTask _task;


    /** Servlet initialization */
    public void init(ServletConfig config) 
            throws ServletException {

        _users = new Hashtable<String, User>();
        _channels = new Hashtable<String, MessagePool>();

        // Creates the user task
        _task = new UserTask();
        _task.start();
    }

    /** 
     * Redirect the call to doPost method 
     * Maybe we want to remove this method in the future
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /** Service method */
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        readInput(request, response);
        setOutput(request, response);
    }


    private void readInput(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String user = request.getParameter("USER");
        if (user == null) {
            return;
        }

        // Register this as a valid user and/or renew user timestamp
        _users.put(user, new User(user));

        // Read the messages as parameters
        Map<String, String[]> map = request.getParameterMap();
        for (String key : map.keySet()) {
            //System.out.println("key: " + key);
            if (!key.equals("USER")) {
                MessagePool pool = getChannel(key);
                String[] texts = map.get(key);
                for (int i = 0; i < texts.length; ++i) {
                    //System.out.println("text: " + texts[i]);
                    pool.addMessage(new Message(user, texts[i]));
                }
            }
        }
    }

    private void setOutput(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String user = request.getParameter("USER");
        if (user == null) {
            return;
        }

        response.setContentType("text/html; charset=iso-8859-1");

        // Buffer de salida
        PrintWriter out = response.getWriter();

        // Send current users
        String users = "USERS";
        for (String u : _users.keySet()) {
            users = users + "\t" + u;
        }
        out.println(users);
        //System.out.println("USERS: " + users);

        // Send its messages
        MessagePool pool = getChannel(user);
        for (Message msg : pool.getMessages()) {
            out.println(msg);
        }
        // Clean its own pool
        pool.clear();

        out.close();
    }

    private MessagePool getChannel(String user) {
        MessagePool pool = _channels.get(user);
        if (pool == null) {
            // Lazy creation
            pool = new MessagePool();
            _channels.put(user, pool);
        }
        return pool;
    }



    /**
     * This task removes all the inactive users. Inactive means ts > 3*TICKS
     */
    private class UserTask extends Thread
    {
        private boolean _done;

        public UserTask() {
            _done = false;
        }

        public void run() {

            while (!_done) {
                // Wait for TICKS ms
                try {Thread.sleep(TICKS);} catch (Exception ex) { }

                // Loop over the user list to remove those inactive
                long ts = System.currentTimeMillis();
                ArrayList<String> obsoleteList = new ArrayList<String>(); 
                Collection<User> values = (Collection<User>) _users.values();
                for (User user : values) {
                    long diff = ts - user.getTimestamp();
                    //System.out.println("user: " + user.getName() + ", diff: " + diff);
                    if (diff > (3*TICKS)) {
                        // Obsolete user
                        obsoleteList.add(user.getName());
                    }
                }
                // Do the actual removal
                for (String user : obsoleteList) {
                    _users.remove(user);
                }
            }
        }

        @SuppressWarnings("unused")
        public void close() {
            _done = true;
        }
    }
}
