package com.osi.chat;

class User
{
	private String _name;
	private long _timestamp;
	
	/** Constructor */
	User(String name) {
		_name = name;
		_timestamp = System.currentTimeMillis();
	}

	public String getName() {
		return _name;
	}
	
	public long getTimestamp() {
		return _timestamp;
	}
	
	@Override
	public String toString() {
		return _name;
	}
}
